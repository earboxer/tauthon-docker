# Docker image for Tauthon

[Tauthon](https://github.com/naftaliharris/tauthon) is a fork of python2
with some features backported from python 3.

This image includes virtualenv, pytest, and pytest-cov for added convenience.
