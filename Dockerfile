FROM buildpack-deps:buster

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8
# https://github.com/docker-library/python/issues/147
ENV PYTHONIOENCODING UTF-8

# extra dependencies (over what buildpack-deps already includes)
RUN apt-get update && apt-get install -y --no-install-recommends \
        tk-dev \
    && rm -rf /var/lib/apt/lists/*

ENV TAUTHON_TAG v2.8.0
ENV TAUTHON_HASH b54fa086dffdbb40ef8b518a4ec64583bee8dfe3

RUN set -ex \
    \
    mkdir -p /usr/src/tauthon \
    && git clone --branch $TAUTHON_TAG https://github.com/naftaliharris/tauthon.git /usr/src/tauthon \
    && cd /usr/src/tauthon \
    && test "$TAUTHON_HASH" = $(git rev-parse HEAD) \
    && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)" \
    && ./configure \
        --build="$gnuArch" \
        --enable-shared \
        --enable-unicode=ucs4 \
    && make -j "$(nproc)" \
    && make install \
    && ldconfig \
    \
    && find /usr/local -depth \
        \( \
            \( -type d -a \( -name test -o -name tests \) \) \
            -o \
            \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
        \) -exec rm -rf '{}' + \
    && rm -rf /usr/src/tauthon \
    \
    && tauthon --version

RUN tauthon -m ensurepip
RUN tauthon -m pip install --no-cache-dir virtualenv pytest pytest-cov

CMD ["tauthon"]
